[Desktop Entry]
Name=KDisplay KDED module

Comment=Automatic display management integrated to the KDE daemon

Type=Service
Icon=preferences-desktop-display-randr
X-KDE-ServiceTypes=KDEDModule
X-KDE-Library=kdisplayd
X-KDE-Kded-autoload=true
X-KDE-Kded-load-on-demand=false
X-KDE-Kded-phase=1
X-KDE-PluginInfo-Version=@KDISPLAY_VERSION@
