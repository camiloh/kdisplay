# Changelog
All notable changes to KDisplay will be documented in this file.
## [5.19.0](https://gitlab.com/kwinft/kdisplay/compare/kdisplay@5.19.0-beta.0...kdisplay@5.19.0) (2020-06-09)

## [5.19.0-beta.0](https://gitlab.com/kwinft/kdisplay/compare/kdisplay@0.0.0...kdisplay@5.19.0-beta.0) (2020-05-24)


### Features

* write configuration files with .json name extension ([51bc9d9](https://gitlab.com/kwinft/kdisplay/commit/51bc9d9bf3869f73c2c277e0b9bea03fcd351fe2))


### Bug Fixes

* **kded:** when no lid-open file exist apply default config ([187bedf](https://gitlab.com/kwinft/kdisplay/commit/187bedf873b076bdd36b050d789084cff5eba42b))
* fall back to scale 1 ([92e11c1](https://gitlab.com/kwinft/kdisplay/commit/92e11c1feed92b849760db611cf9ff059dddb20e))
* on null Config abort in Control ctor ([8d1e7f7](https://gitlab.com/kwinft/kdisplay/commit/8d1e7f7db5e375933ec46044d0c02edd3e919e2b))
* **kded:** set scale by control file ([fc2f875](https://gitlab.com/kwinft/kdisplay/commit/fc2f875e9257bfca0b5701e10d00801da00af016))


### Refactors

* **kded:** change config save path ([bdf4642](https://gitlab.com/kwinft/kdisplay/commit/bdf4642aaf64fc616faa093eabf10e84a920c71c))
* adapt to recent Disman API changes ([815b953](https://gitlab.com/kwinft/kdisplay/commit/815b953a4680575767384aa9a9869c3bf71a2d3b))
* rename project ([6052263](https://gitlab.com/kwinft/kdisplay/commit/60522631dbe20beb44dfc52c6c3575de827af9de))
* save config in map ([5700b25](https://gitlab.com/kwinft/kdisplay/commit/5700b25a169b36468a8ab5ec83abffced6b95431))
